package Model;

import java.util.List;

/** class used as an interface by the controller */
public class Facade
{
    /** current move played */
    private Move move;
    /** symbols used by the players */
    private char[] symbols = new char[] { 'X', 'O' };
    /** player currently playing; can be 0 or 1; default value: 0 */
    private int currentPlayer = 0;
    /** AI used to play vs user */
    private AI ai;


    /**
     * constructor; used to instantiate the facade in the controller
     */
    public Facade()
    {
        // initialize the AI with symbol O and a depth of 2
        this.ai = new AI('O', 2);
    }


    /**
     * resets the current move
     */
    public void resetMove()
    {
        this.move = null;
    }

    /**
     * register the user's chosen option
     * @param coord the option chosen
     */
    public void setChosenOption(Coordinates coord)
    {
        this.move.setChosenOption(coord);
    }

    /**
     * creates / starts a new move from scratch
     * @param coord the initial coordinates of the chosen piece
     */
    public void newMove(Coordinates coord)
    {
        this.move = new Move(coord, getSymbol());
    }

    /**
     * get initial coordinates of the chosen piece
     * @return the initial coordinates
     */
    public Coordinates getChosenPieceInitialCoordinates()
    {
        return this.move.getOrigin();
    }

    /**
     * get the playable options, according to the coordinates of the chosen piece
     * @return an array of coordinates
     */
    public Coordinates[] getPlayableOptions()
    {
        return this.move.getPlayableOptions();
    }

    /**
     * gets the current player's symbol
     * @return the current player's symbol
     */
    public char getSymbol()
    {
        return symbols[currentPlayer];
    }

    /**
     * play the current move
     * @return true if the move could be played, false otherwise
     */
    public boolean play()
    {
        // if move doesn't exist or user didn't choose an option
        if (this.move == null || this.move.getChosenOption() == 0)
            return false;

        // send and apply move to the board
        Board.getInstance().play(move);

        // swap players
        nextPlayer();

        return true;
    }

    /**
     * make the AI play a move
     */
    public void makeAIplay()
    {
        // make the AI play
        Board.getInstance().play(ai.play());
    }

    /**
     * get the winner of the current game
     * @return the symbol of the winner, or a blank space if there is no winner yet
     */
    public char getWinner()
    {
        return Board.getInstance().getWinner();
    }


    /**
     * get the board composed of the symbols only
     * @return a List<List<Character>> representing the current visible state of the board
     */
    public List<List<Character>> getSymbolsBoard()
    {
        return Board.getInstance().getCharBoard();
    }

    /**
     * swaps to the next player
     */
    private void nextPlayer()
    {
        currentPlayer = (currentPlayer + 1) % 2;
    }



    public Move getCurrentMove()
    {
        return move;
    }

    public int getCurrentPlayer()
    {
        return currentPlayer;
    }
}