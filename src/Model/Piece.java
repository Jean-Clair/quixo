package Model;

class Piece
{
    /** symbol of the piece; default: ' ' */
    private char symbol = ' ';


    Piece()
    {
    }



    char getSymbol()
    {
        return symbol;
    }

    void setSymbol(char symbol)
    {
        this.symbol = symbol;
    }
}