package Model;

import java.util.ArrayList;
import java.util.List;

class Board
{
    /** character representing the winner; blank space if no winner yet */
    private char winner = ' ';

    // unique instance of board
    private static Board instance = new Board();

    // actual board
    private List<List<Piece>> board;

    static Board getInstance()
    {
        return instance;
    }

    private Board()
    {
        resetBoard();
    }

    /**
     * resets the board for a new game
     */
    void resetBoard()
    {
        // init the new board
        this.board = new ArrayList<>();

        // init all columns
        for (int i = 0; i < 5; i++)
        {
            this.board.add(new ArrayList<>());

            // fill columns with pieces
            for (int j = 0; j < 5; j++)
                this.getRow(i).add(new Piece());
        }

        // reset the winner
        this.winner = ' ';
    }


    /**
     * applies the given move to the board
     * @param move the move to play
     */
    void play(Move move)
    {
        // if chosen piece has no symbol
        if (getPiece(move.getOrigin()).getSymbol() == ' ')
            // set symbol of the player who made the move
            getPiece(move.getOrigin()).setSymbol(move.getSymbol());

        // coordinates of the piece to move
        Coordinates origin = move.getOrigin();
        // coordinates where to move the piece
        Coordinates destination = move.getDestination();

        // if same line
        if (origin.getX() == destination.getX())
        {
            // get piece to move, then remove it from the list
            Piece piece = getRow(origin.getX()).remove(origin.getY());
            // place the piece to its destination
            getRow(destination.getX()).add(destination.getY(), piece);
        }

        // if same column (origin.getY() == destination.getY())
        else
        {
            // temporary list used to store pieces to swap
            List<Piece> column = new ArrayList<>();

            // pop and store the five pieces on the Yth column
            for (int i = 0; i < 5; i++)
                column.add(getRow(i).remove(origin.getY()));

            // swap origin and destination
            column.add(destination.getX(), column.remove(origin.getX()));

            // push the reordered pieces into the board
            for (int i = 0; i < 5; i++)
                getRow(i).add(destination.getY(), column.get(i));
        }

        // check win condition
        searchAndUpdateWinner(origin, destination);
    }


    /**
     * look for a winner according to the move played
     * @param origin the coordinates of the piece before the move
     * @param destination the coordinates of the piece after the move
     */
    private void searchAndUpdateWinner(Coordinates origin, Coordinates destination)
    {
        // if same row
        if (origin.getX() == destination.getX())
        {
            // check the row
            checkWinnerInList(getRow(origin.getX()));
            // check first column
            checkWinnerInList(getColumn(origin.getY()));
            // check second column
            checkWinnerInList(getColumn(destination.getY()));
        }

        // else: same column
        else
        {
            // check the column
            checkWinnerInList(getColumn(origin.getY()));
            // check first row
            checkWinnerInList(getRow(origin.getX()));
            // check second row
            checkWinnerInList(getRow(destination.getX()));
        }
    }


    /**
     * check if all symbols are the same in a given list, and update winner accordingly
     * @param list the list of pieces to parse
     */
    private void checkWinnerInList(List<Piece> list)
    {
        /* check win condition: full row */

        // get first symbol on the list
        char symbol = list.get(0).getSymbol();

        // iterator; starts at 1 so we won't double check first cell
        int i = 1;
        // for each cell in the row
        for (; i < 5; i++)
            // if symbol is different that the first symbol of the row
            if (list.get(i).getSymbol() != symbol)
                break;

        // if all symbols are the same
        if (i == 5)
            // set winner's symbol
            this.winner = symbol;
    }


    /**
     * invert the given move, then apply it to the board
     * @param move the move to invert
     */
    void revert(Move move)
    {
        // temporary variable used to swap origin and destination
        Coordinates newOrigin = move.getDestination();

        int chosenOption = move.getChosenOption();

        // if option 1
        if (chosenOption == 1)
            // replace option 1 with old origin (new destination)
            move.setOption1(move.getOrigin());
        // if option 2
        else if (chosenOption == 2)
            // replace option 2 with old origin (new destination)
            move.setOption2(move.getOrigin());
        // option 3
        else
            // replace option 3 with old origin (new destination)
            move.setOption3(move.getOrigin());

        // set new origin (old destination)
        move.setOrigin(newOrigin);

        // apply the reverted move
        play(move);
    }


    /**
     * returns a board of characters, corresponding to the symbols of the pieces
     * @return a list of list of Character
     */
    List<List<Character>> getCharBoard()
    {
        // list to return
        List<List<Character>> ret = new ArrayList<>();

        // for each row
        for (int i = 0; i < 5; i++)
        {
            // row to add to the board to return
            List<Character> charRow = new ArrayList<>();
            // get the current row
            List<Piece> pieceRow = getRow(i);

            // for each piece in the row
            for (Piece piece : pieceRow)
                // add the symbol to the list to return
                charRow.add(piece.getSymbol());

            // append the row to the board to return
            ret.add(charRow);
        }

        return ret;
    }


    /**
     * get the winner of the current game
     * @return blank space if no winner, the symbol of the winner otherwise
     */
    char getWinner()
    {
        return winner;
    }


    /**
     * easy-to-use get
     * @param i index of row to return
     * @return the i-th row
     */
    private List<Piece> getRow(int i)
    {
        return this.board.get(i);
    }


    /**
     * gets the j-th column of the board
     * @param j index of the column to return
     * @return the j-th column
     */
    private List<Piece> getColumn(int j)
    {
        // temporary list used to store pieces
        List<Piece> column = new ArrayList<>();

        // store the five pieces on the Yth column
        for (int i = 0; i < 5; i++)
            column.add(getRow(i).get(j));

        return column;
    }


    /**
     * easy-to-use get
     * @param i index of the row
     * @param j index of the column
     * @return the piece at the index [i, j]
     */
    private Piece getPiece(int i, int j)
    {
        return this.board.get(i).get(j);
    }


    /**
     * easy-to-use get
     * @param coord the coordinates of the piece
     * @return the piece at the given coordinates
     */
    private Piece getPiece(Coordinates coord)
    {
        return getPiece(coord.getX(), coord.getY());
    }


}