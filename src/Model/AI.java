package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * artificial intelligence, using the minimax algorithm
 * Source 1: https://www.youtube.com/watch?v=QTw8VJzRW6g
 * Source 2: https://fearyourself.developpez.com/tutoriel/sdl/morpion/part6/
 */
class AI extends Player
{
    /** representation of the board currently evaluated */
    private List<List<Character>> currentBoard;
    /** stack of all moves played */
    private Stack<Move> stack;

    /** minimal value attributed to a losing node */
    private final int MIN = -1000;
    /** maximal value attributed to a losing node */
    private final int MAX = 1000;
    /** depth of the tree to explore in the algorithm; default: 2 */
    private int depth = 4;


    /**
     * default constructor; init a new AI with default symbol (O)
     */
    AI()
    {
        // init new player with symbol 'O'
        super('O');
    }


    /**
     * init a new AI with given symbol
     * @param symbol the symbol to use
     */
    AI(char symbol)
    {
        this();
        this.symbol = symbol;
    }


    /**
     * init a new AI with given symbol
     * @param symbol the symbol to use
     * @param depth the depth of the tree we want to explore
     */
    AI(char symbol, int depth)
    {
        this();
        this.symbol = symbol;
        this.depth = depth;
    }


    /**
     * get the player's move
     * @return move chosen by the AI, using the minmax algorithm
     */
    @Override
    Move play()
    {
        // create a new empty stack
        this.stack = new Stack<>();
        // get the current state of the board
        this.currentBoard = Board.getInstance().getCharBoard();

        // start the AI
        return minimax();
    }


    /**
     * give a score to the given row or column, according to the symbols played in this row or column
     * the more symbols there are of the AI's side, the greater the score will be
     * @param list the row or column to parse
     * @return the value of the row or column (can be negative)
     */
    private int getRowValue(List<Character> list)
    {
        // number of symbols of the AI
        int nbSymbols = 0;

        // count the AI's symbols in the list
        for (char c : list)
            // if cell is not empty
            if (c != ' ')
                // if AI's symbol
                if (c == this.symbol)
                    // increment score
                    ++nbSymbols;
                // if enemy's symbol
                else
                    // decrement score
                    --nbSymbols;

        // score of the row (can be negative)
        return nbSymbols;
    }


    /**
     * give a score to the current board
     * @return the score attributed to the board
     */
    private int evaluate()
    {
        // check if there is a winner in the current board (leaf)
        char winner = checkWinner();

        // if there is a winner
        if (winner != ' ')
        {
            // if winner is the AI
            if (winner == this.symbol)
                // score: max value minus the number of iterations needed to get to that win
                return MAX - stack.size();
            // if AI loses
            else
                // score: min value plus the number of iterations needed to get to that loss
                return MIN + stack.size();
        }

        // score to return
        int score = 0;

        // for each row and column
        for (int i = 0; i < 5; i++)
        {
            // update score with values of each row and column
            score += getRowValue(getRow(i));
            score += getRowValue(getColumn(i));
        }

        // return the score of the current board (current node)
        return score;
    }


    /**
     * iterate through all the possible moves
     * @return the move to play
     */
    private Move minimax()
    {
        // move to play
        Move bestMove = null;
        // best score of all the options tested; initialized with worst possible value
        int scoreMax = MIN;

        // move used to test all the possible options
        Move move;
        // score of the tested option
        int scoreTemp;

        // iterate through the whole tree
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
            {
                // if the cell is playable
                if (i % 4 == 0 || j % 4 == 0)
                {
                    // create new move
                    move = new Move(i, j, symbol);
                    // iterate through all possible options
                    for (int x = 0; x < move.getNbOptions(); x++)
                    {
                        // set option to play
                        move.setChosenOption(x+1);
                        // play the move (apply it to the board)
                        playMoveOnCurrentBoard(move);
                        // evaluate the applied option
                        scoreTemp = calcMin(this.depth - 1);
                        // if it is the best tested option
                        if (scoreTemp > scoreMax)
                        {
                            // store the new max score
                            scoreMax = scoreTemp;
                            // store the new best move
                            bestMove = move;
                        }
                        // revert move before testing the next option
                        revert();
                    }
                }
            }

        return bestMove;
    }


    /**
     * evaluate a min node on the current board
     * @param depth the current depth we're in on the tree
     * @return the score of the current node
     */
    private int calcMin(int depth)
    {
        // if we're at the desired depth
        if (depth == 0)
            // stop iterating through the tree, and evaluate the current node
            return evaluate();

        // check if there's a winner
        char winner = checkWinner();
        // if there's a winner
        if (winner != ' ')
            return evaluate();

        // lowest score of all the options tested; initialized with highest possible value
        int scoreMin = MAX;

        // move used to test all the possible options
        Move move;
        // score of the tested option
        int scoreTemp;

        // iterate through the whole tree
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
            {
                // if the cell is playable
                if (i % 4 == 0 || j % 4 == 0)
                {
                    // create new move
                    move = new Move(i, j, symbol);
                    // iterate through all possible options
                    int nbOptions = move.getNbOptions();
                    for (int x = 0; x < move.getNbOptions(); x++)
                    {
                        // set option to play
                        move.setChosenOption(x+1);
                        // play the move (apply it to the board)
                        playMoveOnCurrentBoard(move);
                        // evaluate the applied option
                        scoreTemp = calcMax(depth - 1);
                        // if it is the best tested option
                        if (scoreTemp < scoreMin)
                        {
                            // store the new max score
                            scoreMin = scoreTemp;
                        }
                        // revert move before testing the next option
                        revert();
                    }
                }
            }

        // return lowest possible score
        return scoreMin;
    }




    private int calcMax(int depth)
    {
        // if we're at the desired depth
        if (depth == 0)
            // stop iterating through the tree, and evaluate the current node
            return evaluate();

        // check if there's a winner
        char winner = checkWinner();
        // if there's a winner
        if (winner != ' ')
            return evaluate();

        // best score of all the options tested; initialized with worst possible value
        int scoreMax = MIN;

        // move used to test all the possible options
        Move move;
        // score of the tested option
        int scoreTemp;

        // iterate through the whole tree
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
            {
                // if the cell is playable
                if (i % 4 == 0 || j % 4 == 0)
                {
                    // create new move
                    move = new Move(i, j, symbol);
                    // iterate through all possible options
                    for (int x = 0; x < move.getNbOptions(); x++)
                    {
                        // set option to play
                        move.setChosenOption(x+1);
                        // play the move (apply it to the board)
                        playMoveOnCurrentBoard(move);
                        // evaluate the applied option
                        scoreTemp = calcMin(depth - 1);
                        // if it is the best tested option
                        if (scoreTemp > scoreMax)
                        {
                            // store the new max score
                            scoreMax = scoreTemp;
                        }
                        // revert move before testing the next option
                        revert();
                    }
                }
            }

        // return best possible score
        return scoreMax;
    }











    /**
     * apply a move to the current board, and push it to the stack
     * @param move the move to play
     */
    private void playMoveOnCurrentBoard(Move move)
    {
        applyMove(move);
        stack.push(move);
    }


    /**
     * applies the given move to the current board
     * @param move the move to play
     */
    private void applyMove(Move move)
    {
        // coordinates of the piece to move
        Coordinates origin = move.getOrigin();
        // coordinates where to move the piece
        Coordinates destination = move.getDestination();

        // if chosen piece has no symbol
        if (getChar(origin) == ' ')
            // set symbol of the player who made the move
            setChar(origin, move.getSymbol());

        // if same line
        if (origin.getX() == destination.getX())
        {
            // get piece to move, then remove it from the list
            char c = getRow(origin.getX()).remove(origin.getY());
            // place the piece to its destination
            getRow(destination.getX()).add(destination.getY(), c);
        }

        // if same column (origin.getY() == destination.getY())
        else
        {
            // temporary list used to store pieces to swap
            List<Character> column = new ArrayList<>();

            // pop and store the five pieces on the Yth column
            for (int i = 0; i < 5; i++)
                column.add(getRow(i).remove(origin.getY()));

            // swap origin and destination
            column.add(destination.getX(), column.remove(origin.getX()));

            // push the reordered pieces into the board
            for (int i = 0; i < 5; i++)
                getRow(i).add(destination.getY(), column.get(i));
        }
    }


    /**
     * look for a winner in the current board, according to the last move played
     */
    private char checkWinner()
    {
        // get origin
        Coordinates origin = stack.peek().getOrigin();
        // get destination
        Coordinates destination = stack.peek().getDestination();

        // temporary variable used to test the current board
        char result;

        // if same row
        if (origin.getX() == destination.getX())
        {
            // check the row
            result = checkWinnerInList(getRow(origin.getX()));
            if (result != ' ')
                return result;

            // check first column
            result = checkWinnerInList(getColumn(origin.getY()));
            if (result != ' ')
                return result;

            // check second column
            result = checkWinnerInList(getColumn(destination.getY()));
            if (result != ' ')
                return result;
        }

        // else: same column
        else
        {
            // check the column
            result = checkWinnerInList(getColumn(origin.getY()));
            if (result != ' ')
                return result;
            // check first row
            result = checkWinnerInList(getRow(origin.getX()));
            if (result != ' ')
                return result;
            // check second row
            result = checkWinnerInList(getRow(destination.getX()));
            if (result != ' ')
                return result;
        }

        // last case: no winner yet
        return ' ';
    }


    /**
     * check if all symbols are the same in a given list, and update winner accordingly
     * @param list the list of pieces to parse
     */
    private char checkWinnerInList(List<Character> list)
    {
        /* check win condition: full row */

        // get first symbol on the list
        char symbol = list.get(0);

        // iterator; starts at 1 so we won't double check first cell
        int i = 1;
        // for each cell in the row
        for (; i < 5; i++)
            // if symbol is different that the first symbol of the row
            if (list.get(i) != symbol)
                break;

        // if all symbols are the same
        if (i == 5)
            // set winner's symbol
            return symbol;

        return ' ';
    }


    /**
     * cancel the last move played, and pop it from the stack
     */
    private void revert()
    {
        // get the last move played, and pop it from the stack
        Move move = stack.pop();

        // temporary variable used to swap origin and destination
        Coordinates newOrigin = move.getDestination();

        int chosenOption = move.getChosenOption();

        // if option 1
        if (chosenOption == 1)
            // replace option 1 with old origin (new destination)
            move.setOption1(move.getOrigin());
            // if option 2
        else if (chosenOption == 2)
            // replace option 2 with old origin (new destination)
            move.setOption2(move.getOrigin());
            // option 3
        else
            // replace option 3 with old origin (new destination)
            move.setOption3(move.getOrigin());

        // set new origin (old destination)
        move.setOrigin(newOrigin);

        // apply the reverted move
        applyMove(move);
    }





    /**
     * get the i-th row of the current board
     * @param i index of row to return
     * @return the i-th row
     */
    private List<Character> getRow(int i)
    {
        return this.currentBoard.get(i);
    }


    /**
     * get the j-th column of the current board
     * @param j index of the column to return
     * @return the j-th column
     */
    private List<Character> getColumn(int j)
    {
        // temporary list used to store pieces
        List<Character> column = new ArrayList<>();

        // store the five pieces on the Yth column
        for (int i = 0; i < 5; i++)
            column.add(getRow(i).get(j));

        return column;
    }


    /**
     * easy-to-use get
     * @param i index of the row
     * @param j index of the column
     * @return the character at the index [i, j]
     */
    private char getChar(int i, int j)
    {
        return this.currentBoard.get(i).get(j);
    }


    /**
     * easy-to-use get
     * @param coord the coordinates of the piece
     * @return the character at the given coordinates
     */
    private char getChar(Coordinates coord)
    {
        return getChar(coord.getX(), coord.getY());
    }


    private void setChar(Coordinates coord, char character)
    {
        this.currentBoard.get(coord.getX()).set(coord.getY(),character);
    }
}