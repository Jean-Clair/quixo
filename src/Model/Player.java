package Model;

abstract class Player
{
    /** symbol of the AI; default: O */
    protected char symbol;


    /**
     * init a new player with the given character as his symbol
     */
    Player(char symbol)
    {
        this.symbol = symbol;
    }

    /**
     * get the player's move
     * @return move chosen by the player
     */
    abstract Move play();

    char getSymbol()
    {
        return symbol;
    }
}