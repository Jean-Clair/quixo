package Model;

public class Move
{
    /** coordinates of the piece before being moved */
    private Coordinates origin;
    /** playable option 1 */
    private Coordinates option1;
    /** playable option 2 */
    private Coordinates option2;
    /** playable option 3 */
    private Coordinates option3;
    /** option chosen by the player; 0 = reset (cancel the move) */
    private int chosenOption;
    /** determines whether there are 2 or 3 playable options */
    private boolean isCorner;
    /** symbol of the player who made the move */
    private char symbol;

    /**
     * create a move with the chosen piece
     * @param pieceCoord coordinates of the chosen piece
     * @param symbol the symbol of the current player
     */
    Move(Coordinates pieceCoord, char symbol)
    {
        this.origin = pieceCoord;
        this.symbol = symbol;
        setPlayableOptions();
    }


    /**
     * create a move with the chosen piece
     * @param x abscissa (coordinate x) of the chosen piece
     * @param y ordinate (coordinate y) of the chosen piece
     * @param symbol the symbol of the current player
     */
    Move(int x, int y, char symbol)
    {
        this(new Coordinates(x, y), symbol);
    }


    /**
     * set playable options, according to the input move
     */
    private void setPlayableOptions()
    {
        // piece chosen by the player
        int x = this.origin.getX();
        int y = this.origin.getY();

        // if move played in corner
        if (x % 4 == 0 && y % 4 == 0)
        {
            // only two playable options
            this.setOption(x == 0 ? 4 : 0, y);
            this.setOption(x, y == 0 ? 4 : 0);
            // set option3 with the same value (easier to handle in the controller)
            // this.setOption(option1);
        }

        else
        {
            // if (x == 0 || x == 4)
            if (x % 4 == 0)
            {
                this.setOption(x, 0);
                this.setOption(x, 4);
                this.setOption(x == 0 ? 4 : 0, y);
            }
            // else: x != 0 && y % 4 == 0
            else
            {
                this.setOption(0, y);
                this.setOption(4, y);
                this.setOption(x, y == 0 ? 4 : 0);
            }
        }
    }


    /**
     * set option to the first available slot
     * @param coord the coordinates to set
     */
    private void setOption(Coordinates coord)
    {
        if (option1 == null)
            option1 = coord;
        else if (option2 == null)
        {
            option2 = coord;
            isCorner = true;
        }
        else
        {
            option3 = coord;
            isCorner = false;
        }
    }

    /**
     * set option to the first available slot
     * @param x abscissa (coordinate x)
     * @param y ordinate (coordinate y)
     */
    private void setOption(int x, int y)
    {
        setOption(new Coordinates(x, y));
    }


    /**
     * changes the move selected by the player (and thus the playable options)
     * @param coord coordinates of the new move
     */
    void changeMove(Coordinates coord)
    {
        this.origin = coord;
        // recalculate the playable options
        setPlayableOptions();
    }


    /**
     * changes the move selected by the player (and thus the playable options)
     * @param x abscissa (coordinate x)
     * @param y ordinate (coordinate y)
     */
    public void changeMove(int x, int y)
    {
        changeMove(new Coordinates(x, y));
    }


    /**
     * returns an array containing all the playable options (2 or 3 elements)
     * @return an array of coordinates
     */
    Coordinates[] getPlayableOptions()
    {
        // if only 2 options
        if (option3 == null)
            return new Coordinates[] { option1, option2 };

        // if 3 playable options
        return new Coordinates[] { option1, option2, option3 };
    }

    /**
     * get the number of playable options
     * @return number of playable options (2 or 3)
     */
    int getNbOptions()
    {
        return this.option3 == null ? 2 : 3;
    }

    /**
     * returns the coordinates of the chosen option
     * @return coordinates of the chosen option
     */
    Coordinates getDestination()
    {
        return getPlayableOptions()[chosenOption - 1];
    }


    Coordinates getOrigin()
    {
        return origin;
    }

    void setOrigin(Coordinates origin)
    {
        this.origin = origin;
    }

    Coordinates getOption1()
    {
        return option1;
    }

    void setOption1(Coordinates option1)
    {
        this.option1 = option1;
    }

    Coordinates getOption2()
    {
        return option2;
    }

    void setOption2(Coordinates option2)
    {
        this.option2 = option2;
    }

    Coordinates getOption3()
    {
        return option3;
    }

    void setOption3(Coordinates option3)
    {
        this.option3 = option3;
    }

    int getChosenOption()
    {
        return chosenOption;
    }

    void setChosenOption(int chosenOption)
    {
        this.chosenOption = chosenOption;
    }

    void setChosenOption(Coordinates coord)
    {
        if (coord.equals(option1))
            this.chosenOption = 1;
        else if (coord.equals(option2))
            this.chosenOption = 2;
        else if (option3 != null && coord.equals(option3))
            this.chosenOption = 3;
    }

    boolean isCorner()
    {
        return isCorner;
    }

    char getSymbol()
    {
        return symbol;
    }

    void setSymbol(char symbol)
    {
        this.symbol = symbol;
    }

    public String toString()
    {
        String ret = String.format("initial move: %s; option 1: %s; option 2: %s; ",
                origin.toString(), option1.toString(), option2.toString());

        if (option3 != null)
            ret += "option 3: " + option3.toString() + "; ";
        else
            ret += "no option 3; ";

        ret += "chosen option: ";
        if (chosenOption != 0)
            ret += chosenOption;
        else
            ret += "none";

        return ret;
    }
}