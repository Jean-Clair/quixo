package Controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.event.ActionEvent;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import Model.Facade;
import Model.Coordinates;

public class BoardController implements Initializable
{
    /** JavaFX's programmatic representation of the grid */
    @FXML
    GridPane fxGrid;
    /** label used to display the winner */
    @FXML
    Label fxWinnerLabel;
    /** facade used to access the model */
    private Facade facade = new Facade();
    /** determines whether we're playing vs the AI or another human; default: true */
    private boolean enableAI = false;


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        // set catchMove on action for all buttons in the grid
        for (int i = 0; i < 25; i++)
            getButton(i).setOnAction(this::catchMove);
    }

    /**
     * function called by the grid's buttons on click
     * @param event the event which triggered the function call
     */
    @FXML
    public void catchMove(ActionEvent event)
    {
        // get the clicked button
        Button b = (Button)event.getSource();
        // get the button's coordinates
        Coordinates coord = getButtonCoord(b);

        // get button's background color
        Color color = (Color)b.getBackground().getFills().get(0).getFill();

        // if button's color is red
        if (color.equals(Color.RED))
        {
            // reset move
            facade.resetMove();
            // reset colors
            resetGridColors();
        }

        // if button color is green
        else if (color.equals(Color.GREEN))
        {
            // set chosen option
            facade.setChosenOption(coord);
            // play the move
            playMove();
            // reset board colors
            resetGridColors();
        }

        // if button uncolored
        else
        {
            // reset colors (in case of overwriting move)
            resetGridColors();
            // set new move
            facade.newMove(coord);
            // edit the grid
            editGridWithMove();
        }
    }

    /**
     * recolor clicked button's playable options
     */
    private void editGridWithMove()
    {
        /* color clicked and playable buttons */

        // variable used to work on buttons
        Button b;

        // get clicked button
        b = getButton(facade.getChosenPieceInitialCoordinates());
        // color it
        b.setStyle("-fx-background-color: RED;");

        // get playable options
        Coordinates[] playableOptions = facade.getPlayableOptions();

        // for each option
        for (Coordinates coord : playableOptions)
        {
            // get button
            b = getButton(coord);
            // color it
            b.setStyle("-fx-background-color: GREEN;");
        }
    }


    /**
     * reset colors
     */
    private void resetGridColors()
    {
        for (Node n : fxGrid.getChildren())
            n.setStyle("");
    }


    /**
     * play the input move and update the board
     */
    private void playMove()
    {
        // if the move was correct, thus has been played (and if the AI has responded if needed)
        if (facade.play())
            updateBoard();

        // get winner
        char winner = facade.getWinner();

        // if there is a winner
        if (winner != ' ')
            // display win message and disable whole board
            win(winner);

        // if game not ended and playing vs AI
        else if (enableAI)
        {
            // make the AI play
            facade.makeAIplay();
            // display move
            updateBoard();
        }

    }


    /**
     * update the displayed board with the model's board
     */
    private void updateBoard()
    {
        // get chars board
        List<List<Character>> charBoard = facade.getSymbolsBoard();

        // for each symbol
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
                getButton(i, j).setText(charBoard.get(i).get(j).toString());
    }


    /**
     * display win message and disable whole board
     * @param winnerSymbol the winner's symbol
     */
    private void win(char winnerSymbol)
    {
        // edit label text
        fxWinnerLabel.setText("Winner: " + winnerSymbol);
        // make it visible
        fxWinnerLabel.setVisible(true);

        // disable all buttons
        for (int i = 0; i < 25; i++)
            getButton(i).setDisable(true);
    }


    /**
     * switch between player vs player and player vs AI
     */
    @FXML
    private void switchMode()
    {
        this.enableAI = !this.enableAI;
    }


    /**
     * calculates the coordinates of a given button in the grid
     * @param b the button to get the coordinates of
     * @return  a Coordinates object
     */
    private Coordinates getButtonCoord(Button b)
    {
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
            {
                Button btn = getButton(i, j);
                if (btn.getId().equals(b.getId()))
                    return new Coordinates(i, j);
            }

        return null;
    }

    /**
     * get the button at the Xth row and the Yth column
     * @param x the row to get the button from
     * @param y the column to get the button from
     * @return  the button from the grid
     */
    private Button getButton(int x, int y)
    {
        // x*5: grid of 5*5 squares
        return (Button)fxGrid.getChildren().get(x*5 + y);
    }

    /**
     * get the button at the Xth row and the Yth column
     * @param coord the coordinates of the button to get
     * @return  the button from the grid
     */
    private Button getButton(Coordinates coord)
    {
        // x*5: grid of 5*5 squares
        return (Button)fxGrid.getChildren().get(coord.getX()*5 + coord.getY());
    }

    /**
     * get the Xth button in the grid
     * @param x the index of the button to get
     * @return  the Xth button
     */
    private Button getButton(int x)
    {
        return (Button)fxGrid.getChildren().get(x);
    }


    /**
     * prints the string representation of an object to the console; used for debug purposes only
     * @param o the object to print
     */
    private void printDebug(Object o)
    {
        System.out.println(o.toString());
    }
}